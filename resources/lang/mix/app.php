<?php

return [
    'success' => 'success نجاح',
    'invalid_credentials' => 'wrong E-mail or password البريد الالكترونى او كلمة المرور خطأ',
    'data_required' => 'required data البيانات ناقصه',
    'email_exists' => 'email already exists البريد الالكترونى موجود من فبل ',
    'mail_exception' => 'error in send email خطأ فى ارسال البريد الالكترونى',
    'token_error' => 'verification code is error كود التاكيد خطأ',
    'reset_token_error' => 'reset password code is error كود استعاده كلمة المرور خطأ',
    'error_update' => 'error happened on update حدث خطأ اثناء التحديث',
    'create_error' => 'error happened on insert حدث حطأ اثناء الاضافة',
    'pdo_exception' => 'unexpected error حدث خطأ غير متوقع',
    'unique_data' => 'data unique موجود من قبل',
    'file_note_image' => 'un expected uploaded file الملف المرفوع غير مدعوم',
    'error_delete' => 'error happened on delete حدث حطأ أثناء الحذف',
    'invalid_token' => 'auth token was invalid التوكن غير موجود',
    'user_not_found' => 'user not found المستخدم غير موجود',
    'token_expired' => 'auth token was expired التوكن غير صالح',
    'could_not_create_token' => 'could not create token غير قادر ع انشاء التوكن',
    'email_not_found' => 'Email address not found البريد الالكترونى غير موجود',
    'allow_extention_error' => 'the extension of file is not supported الملف المرفوع غير مدعوم',
    'unable_upload_file' => 'unable to upload this file حدث خطأ اثناء رفع الملف',
    'must_be_file' => 'this field must be uploaded file الملف المرفوع لابد ان يكون ملف',
    'user_already_activated' => 'user already activated المستخدم مفعل بالفعل',
    'current_password_error' => 'current password error خطأ فى كلمه المرور الحاليه',
    'unexpected_error' => 'unexpected error حدث حطأ فى السيرفر',
    'add_days_error' => 'error happened when add days for this job حدث حطأ اثناء اضافة الايام للوظيفه',
    'user_not_activated' => 'your account is not verified yet المستخدم غير مفعل',
    'not_found' => 'not found غير موجود',
    'invalid_credentials_seeker' => 'wrong mobile number or password رقم الهاتف او كلمة المرور خطأ',
    'code_error' => 'code error خطأ ف الكود',
    'mobile_exists' => 'The mobile number has already been taken رقم الهاتف موجود بالفعل',
    'fav_already_exists' => 'job already exists in favorites list الوظيفه موجوده بالفعل فى المفضله',
    'same_old_mobile' => 'you enter your old mobile number برجاء ادخال رقم هاتف غير الرقم الحالى',
    'already_applied' => 'you already apply for this job before تم التقديم ع هذه الوظيفه من قبل',
    'account_need_active' => 'this account need to activated هذا الحساب محتاج للتفعيل',
    'account_need_complete_register' => 'this account need to complete registration data هذا الحساب محتاج تكمله البيانات الشخصيه',
    'job_closed' => 'this job is closed تم غلق الوظيفة',
];
