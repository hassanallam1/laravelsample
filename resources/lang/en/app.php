<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => 'success',
    'invalid_credentials' => 'wrong E-mail or password',
    'data_required' => 'required data',
    'email_exists' => 'email already exists',
    'mail_exception' => 'error in send email',
    'token_error' => 'verification code is error',
    'reset_token_error' => 'reset password code is error',
    'error_update' => 'error happened on update',
    'create_error' => 'error happened on insert',
    'pdo_exception' => 'unexpected error',
    'unique_data' => 'data unique',
    'file_note_image' => 'un expected uploaded file ',
    'error_delete' => 'error happened on delete',
    'invalid_token' => 'auth token was invalid',
    'user_not_found' => 'user not found ',
    'token_expired' => 'auth token was expired',
    'could_not_create_token' => 'could not create token',
    'email_not_found' => 'Email address not found',
    'allow_extention_error' => 'the extension of file is not supported ',
    'unable_upload_file' => 'unable to upload this file',
    'must_be_file' => 'this field must be uploaded file',
    'user_already_activated' => 'user already activated',
    'current_password_error' => 'current password error',
    'unexpected_error' => 'unexpected error',
    'add_days_error' => 'error happened when add days for this job',
    'user_not_activated' => 'your account is not verified yet',
    'not_found' => 'not found ',
    'invalid_credentials_seeker' => 'wrong mobile number or password ',
    'code_error' => 'code error ',
    'mobile_exists' => 'The mobile number has already been taken',
    'fav_already_exists' => 'job already exists in favorites list',
    'same_old_mobile' => 'you enter your old mobile number',
    'already_applied' => 'you already apply for this job before',
    'account_need_active' => 'this account need to activated',
    'account_need_complete_register' => 'this account need to complete registration data',
    'job_closed' => 'this job is closed',
    'must_add_cv' => 'Must add cv to able to apply jobs',
    'user_same_password' => 'You cannot use same current password',
    'unable_upload_file_image' => 'error happened in creating map image',
    'user_admin_deactivate' => 'your Account In active please contact with Khtwteen',
];
