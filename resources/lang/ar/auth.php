<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'page-title' => 'تسجيل الدخول',
    'login_empty_error' => 'حدث خطأ ! برجاء تصحيح الاخطأء',
    'email' => 'البريد الاكترونى',
    'password' => 'كلمة المرور',
    'remember' => 'تذكرنى',
    'forget' => 'نسيت كلمة المرور ؟',
    'login' => 'دخول',
    'welcome-adminpanel' => 'مرحبا بكم فى لوحة التحكم',
    'return_home' => 'الرجوع للموقع الرئيسى',
    'login-placeholder' => 'البريد الالكترونى او رقم البطاقة',
    'doctor_login' => 'تسجيل دخول دكتور',

];
