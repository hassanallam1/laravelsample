<?php

namespace App\Http\Controllers;

use App\Helpers\ApiFunctions;
use App\Repositories\CategoriesRepository;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Config;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Validator;

class GeneralController extends Controller
{

    use AuthorizesRequests,
        DispatchesJobs,
        ValidatesRequests,
        ApiFunctions;

    public $user;
    public $request;
    public $categoryRepo;
    public $areaRepo;
    public $sectionRepo;
    public $projectRepo;
    public $imageRepo;

    /**
     * GeneralController constructor.
     * @param Request $request
     * @param CategoriesRepository $categoryRepo
     */
    public function __construct(Request $request ,CategoriesRepository $categoryRepo)
    {
        $this->categoryRepo=$categoryRepo;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories(Request $request){

        $cats = $this->categoryRepo->all(['*']);
        $data=[];
        foreach ($cats as $row){
            $data[]=[
                'id'=>$row->ProTyp_ID,
                'title'=>$row->ProTyp_Name,
            ];
        }
        return $this->outApiJson('success','',$data);
    }

}
