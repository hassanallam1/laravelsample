<?php

namespace App\Http\Middleware;

use Closure;
class SetSettings
{

    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check for lang
        if($request->header('lang')){

            $lang=$request->header('lang');
        }else{
            $lang=app()->getLocale();
        }
        app()->setLocale($lang);
        return $next($request);
    }

}
