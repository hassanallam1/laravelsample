<?php

namespace App\Helpers;

use Intervention\Image\Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Trait Functions
 * @package App\Helpers
 *
 */
trait ApiFunctions {

    /**
     * @param $file
     * @param $type
     * @param $folder
     * @return array
     */
    public function uploadFile($file) {
        try {
            $ext = strtolower($file->getClientOriginalExtension());
            if (!in_array(strtolower($ext), $this->getExt())) {
                return [false, 'allow_extention_error'];
            }
            $subfolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
            $destinationPath = storage_path('app/public/uploads/'. $subfolder);
            $fileName = md5($file->getClientOriginalName()) . '-' . rand(9999, 9999999) .
                    '-' . rand(9999, 9999999) . '.' . $file->getClientOriginalExtension();
            $file->move($destinationPath, $fileName);
            return [true, $subfolder . $fileName];
        } catch (FileException $exception) {
            return [false, 'unable_upload_file'];
        }
    }

    public function getMapImage($lat,$lng) {
        try {
            $subfolder = date('Y') . '/' . date('m') . '/' . date('d') . '/';
            $destinationPath = storage_path('app/public/uploads/'. $subfolder);
            if(!\File::isDirectory($destinationPath)) {
                \Storage::makeDirectory('public/uploads/' . $subfolder);
            }
            $path = 'https://maps.googleapis.com/maps/api/staticmap?zoom=15&size=720x200&maptype=roadmap&markers=icon:http://52.174.22.188/khatwteen/public/storage/uploads/2019/01/01/75a6780cb15fb7a24f9a9cad3ac82b6c-2776038-8659205.png|'.$lat.','.$lng.'&style=feature:administrative%7Celement:geometry%7Cvisibility:off&style=feature:poi%7Cvisibility:off&style=feature:road%7Celement:labels.icon%7Cvisibility:off&style=feature:transit%7Cvisibility:off&key=AIzaSyDKO6nlu7vkrDrjP0-JX-1i-c78X7M1zz8';
            $fileName = md5(rand(9999, 9999999)) .'-' . rand(9999, 9999999) . '.' .'jpg';
            \Image::make($path)->encode('jpg')->encode('jpg')->save($destinationPath.$fileName);
            return [true, $subfolder . $fileName];
        } catch (FileException $exception) {
            return [false, 'unable_upload_file_image'];
        }
    }

    /**
     * @return array
     */
    public function getExt() {
        return ['txt', 'doc', 'docx', 'xls', 'xlsx', 'pdf','gif', 'jpg', 'jpeg', 'png'];
    }

    /**
     * @param bool $status
     * @return array|mixed
     */
    public function statusCodes($status = false) {
        $array = [
            'success' => 200,
            'invalid_credentials' => 400,
            'data_required' => 440,
            'email_exists' => 402,
            'mail_exception' => 403,
            'token_error' => 404,
            'reset_token_error' => 405,
            'api_secret_error' => 406,
            'error_update' => 407,
            'create_error' => 408,
            'pdo_exception' => 409,
            'unique_data' => 410,
            'file_note_image' => 411,
            'error_delete' => 412,
            'invalid_token' => 413,
            'user_not_found' => 414,
            'token_expired' => 415,
            'could_not_create_token' => 416,
            'email_not_found' => 417,
            'allow_extention_error' => 418,
            'unable_upload_file' => 419,
            'must_be_file' => 420,
            'current_password_error' => 422,
            'unexpected_error' => 423,
            'user_not_activated' => 425,
            'not_found' => 426,
            'code_error' => 428,
            'user_same_password' => 437,
            'unable_upload_file_image' => 438,
            'select_one_permission' => 439,
            'permission_not_exists' => 440,
            'role_exists' => 441,
            'role_not_allowed_to_inactive' => 442,
            'must_attendance_exist' => 443,
            'worker_has_no_arrival' => 444,
            'worker_arrival_before' => 445,
            'worker_leave_before' => 446,
            'user_admin_deactivate' => 401,
            'user_has_no_role' => 502,
            'worker_blocked' => 510,
            'worker_not_exists' => 511,
            'worker_has_no_project' => 512,
            'worker_blocked_site' => 513,
            'worker_work_in_other_project' => 514,
        ];
        if ($status) {
            return $array[$status];
        }
        return $array;
    }


    /**
     * @param $statusCode
     * @param $message
     * @param array $data
     * @param int $responseStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function outApiJson($statusCode, $message='', $data = null, $responseStatus = 200)
    {
        $message=$message?$message:trans('app.'.$statusCode);
        try {
            $outData = [];

            $outData['code'] =$this->statusCodes($statusCode);
            $outData['message'] = $message;
            $outData['data'] = $data;
            return response()->json($outData, $responseStatus);
        }catch (\Throwable  $ex){
            dd(1);
            return response()->json($outData, $responseStatus);
        }
    }

    /**
     * @param $errors
     * @return array
     */
    public function getValidationMessages($errors){
        $data=[];
        foreach ($errors->toArray() as $key=>$value){
            foreach ($value as $row){
                $data[]=$row;
            }
        }
        return $data;
    }

    /**
     * @param $user
     * @return array
     */
    public function getUserObject($user){
        $data=[
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'mobile' => $user->mobile,
            'code' => $user->code,
            //'avatar' => ($user->avatar)?asset('storage/uploads'.$user->avatar):'',
        ];
        return $data;
    }

    /**
     * @param $user
     * @return array
     */
    public function getAdminObject($user){
        $data=[
            'id' => $user->id,
            'email' => $user->email,
            'name' => $user->name,
            'mobile' => $user->mobile,
            'code' => $user->code,
            'role' => $user->roles->count()>0?$user->roles[0]->name:'',
            'role_id' => $user->roles->count()>0?$user->roles[0]->id:'',
            'permissions' => $user->roles->count()>0?$user->roles[0]->permissions->pluck('name'):'',

        ];
        return $data;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function getExpText($key){
        $data=[
            1 => '0 : 2',
            2 => '2 : 5',
            3 => '5 : 10',
            4 => ' > 10',
        ];
        return $data[$key];
    }

    public function getTeamText($key){
        $data=[
            1 => '0:10',
            2 => '10:50',
            3 => '50:100',
            4 => 'more than 100',
        ];
        return $data[$key];
    }
}
