<?php

namespace App\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Repositories\Criteria\AdvancedSearchCriteria;
class CategoriesRepository extends Repository
{

    public function model()
    {
        return 'App\Models\Categories';
    }

    public function filterData($request) {
        //start to search data
        $where_obj = new \App\Repositories\Criteria\WhereObject();
        //start sort
            $where_obj->pushOrder('id', 'desc');
        if($request->input('keyword')){
            $where_obj->pushWhere('title', $request->input('keyword'), 'contain');
        }
        // push advanced search
        $push = new \App\Repositories\Criteria\AdvancedSearchCriteria;
        $push::setWhereObject($where_obj);
        // push criteria to repository
        $this->pushCriteria(new AdvancedSearchCriteria());
        $paginate = $this->all(['*']);
        $items = $paginate;
        return $items;
    }
}
