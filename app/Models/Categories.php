<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Categories
 * @package App\Models
 */
class Categories extends Model
{

    protected $guarded = ['ProTyp_ID'];
    protected $table = 'TWEBWF_ProjectTypes';
    protected $dateFormat = 'Y-m-d H:i:s';
}
